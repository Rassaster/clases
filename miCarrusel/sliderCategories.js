import { Rasslider } from "./Rasslider.js";
const sliderCategories = new Rasslider ({
  sliderJumps         : 3,
  prevBtnId           : "#sliderCategories_prevButton",
  nextBtnId           : "#sliderCategories_nextButton",
  wrapperId           : "#sliderCategories_wrapper",
  paginatorCtnId      : "#sliderCategories_paginator",
  slideClass          : ".Rasslider__wrapper__element",
  paginatorsClassName : "section__showcaseCategories__slider__paginator__circle"
});

sliderCategories.createPaginator();
sliderCategories.previousSlide();
sliderCategories.nextSlide();
sliderCategories.paginationJumper();